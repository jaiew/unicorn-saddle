# Unicorn Saddle

This is a simple setup script for getting a dev unicorn instance up and running.

So far it installs the following:

* Apache (setup as a service)
* Atlassian Plugin SDK (comes with mvn).
* Configures JAVA_HOME
* Sets up _unicorn-service_ script.

##Quick Setup:

Copy the command below and run it on your unicorn instance.

    curl -s https://bitbucket.org/jaiew/unicorn-saddle/raw/master/scripts/saddle.sh | sh && source ${HOME}/.bashrc

**Already Available on Unicorn**

* git
* svn
* hg
* python
* ruby

- - -

**NOTE**: Once a command has been run it shouldn't run again if the script saddle.sh script is run again. Only new commands that haven't been run should execute.

##Managing Services

The saddle setup script sets up a _unicorn-service_ script for making it easier to manage services on unicorn.
It allows you to create, list, delete, stop, start and restart services.

### Create a service

    unicorn-service create <name> [optional-user]

The above command will create a new service named after the parameter _name_. It will be created in /data/_name_ and a symlink will be created in /services/_name_ back to it.

If an optional user name is passed it will create the service to be run as this user. If the user doesn't exist the user will be created. The new service will then handle ensuring that every time it is run that the user exists.

In addition all of the bolierplate work for setting up logging a logging service for your service is taken care of for you.

    > unicorn-service create test1 bob
    Created user 'bob' with uid: 8000
    creating service: 'test1' for user: bob
    service: test1 has been created at /service/test1
    
### Remove a service

    unicorn-service remove <name> [--complete|-c]
    
The above command can be used to remove a service. This takes care of cleaning up the service symlink properly and making sure that the service isn't left in an invalid state.

If the optional _--complete_ or _-c_ parameters are passed then this will clean up the service completely also removing the /data/_name_ directory.

    > unicorn-service remove test1 -c
    This will remove the service 'test1'. Are you suer [y/n]? y
    Removing service 'test1'
    Service 'test1' removed.
    Completely removed service 'test1'

### List services

    unicorn-service list
   
The above command will list all services and their status.

    > unicorn-service list
    Listing services
    sys_apache [system service] - up (pid 1461) 21052 seconds 
    sys_gmond [system service] - up (pid 1362) 21254 seconds 
    sys_sshd [system service] - up (pid 1360) 21254 seconds 
    test1 - down 34 seconds, normally up 
    test2 - up (pid 14241) 4 seconds 
    test3 - up (pid 14172) 11 seconds
    
### start, stop, restarting services

    unicorn-service <stop|start|restart> <name>
    
The above commands handle stopping, starting and restarting services.

    > unicorn-service restart test2
    restarting 'test2'.
    
    > unicorn-service stop test2
    Stopping service 'test2'.
    
    > unicorn-service start test2
    Starting service 'test2'
