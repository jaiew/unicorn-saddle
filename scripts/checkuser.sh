checkUserAndCreate () {
  user=$1
  uidparam=$2

  UID_START=8000

  check=`grep "^${user}:" /etc/passwd`

  if [ "${check}" == "" ]
  then

    if [ "${uidparam}" != "" ]
    then
      useradd -u ${uidparam} -d /data/home/${user} ${user} &> /dev/null
      success=$?

      if [ $success -ne 0 ]
      then
        echo "Failed to create user '${user}' with uid: ${uidparam}"
        exit 1
      fi
    fi

    done=1
    uid=${UID_START}
    while [ ${done} -ne 0 ]
    do
      useradd -u ${uid} -d /data/home/${user} ${user} &> /dev/null
      done=$?

      if [ $done -ne 0 ]
      then
        uid=`expr ${uid} + 1`
      fi
    done

    echo "Created user '${user}' with uid: ${uid}"

  else
    echo "user ${user} exists"
  fi
}
