#!/bin/sh

if [  $(id -u) != "0" ]; then
  echo "This script must be executed as root" 1>&2
  exit 1
fi

BASH_RC=""
BASH_PATH=""
TMP_DIR=/data/tmp
HOME_BIN=${HOME}/bin
ATL_PLUGIN_SDK_VERSION="3.7"
ATL_PLUGIN_SDK="https://maven.atlassian.com/public/com/atlassian/amps/atlassian-plugin-sdk/${ATL_PLUGIN_SDK_VERSION}/atlassian-plugin-sdk-${ATL_PLUGIN_SDK_VERSION}.tar.gz?eventtype=download_plugin_sdk"

REPO_URL="https://bitbucket.org/jaiew/unicorn-saddle"
SETTINGS_XML_PATCH="${REPO_URL}/raw/master/patches/settings.xml.patch"
SETTINGS_XML_PROXY="${REPO_URL}/raw/master/files/settings-proxy.xml"

APACHE_SAMPLE_CONF="${REPO_URL}/raw/master/config/apache/instance.sample.conf"
APACHE_INCLUDE_SAMPLE_CONF="${REPO_URL}/raw/master/config/apache/include.instance.sample.conf"

SAMPLE_INDEX_HTML_FILE="${REPO_URL}/raw/master/config/apache/index.html"
CHECK_USER_SCRIPT="${REPO_URL}/raw/master/scripts/checkuser.sh"
UNICORN_SERVICE_SCRIPT="${REPO_URL}/raw/master/scripts/unicorn-service"

INSTALL_CHECK_FILE="${HOME}/.unicorn-saddle"

log () { printf "$*\n"; return $? ; }

fail () { log "\nERROR: $*\n" ; exit 1 ; }

qpushd () {
  pushd "$1" &> /dev/null
}

qpopd () {
  popd &> /dev/null
}


cmd_exists () {
  type "$1" &> /dev/null;
}

getFile () {
  output="$1"
  url="$2"

  log "Retrieving file ${output} from ${url}"

  if cmd_exists wget
  then
    wget --quiet -O "$output" "$url"
  else
    curl --silent -o "$output" "$url"
  fi
}

untarGz () {
  name=$1
  log "Unpacking ${name}.tar.gz"
  tar -xvzf "${name}.tar.gz"
}

run () {
  cmd=$1

  val=`cat ${INSTALL_CHECK_FILE} | grep "${cmd}"`

  if [ "${val}" == "" ]
  then
    echo "Running: ${cmd}"
    ${cmd}
    echo "${cmd}" >> "${INSTALL_CHECK_FILE}"
  else
    echo "Skipping ${cmd}. Run already."
  fi

}


commitToBashRC () {
  if [ "${BASH_RC}" != "" ]
  then
    echo "" >> $HOME/.bashrc
    printf "${BASH_RC}" >> $HOME/.bashrc
    printf "" >> $HOME/.bashrc
  fi
}

addToBashRC () {
  contents=$1
  BASH_RC="${BASH_RC}\n"
  BASH_RC="${BASH_RC}$contents"
}

addToPath () {
  contents=$1
  if [ "${BASH_PATH}" == "" ]
  then
    BASH_PATH="${contents}"
  else
    BASH_PATH="${BASH_PATH}:${contents}"
  fi
}

addPathToBashRC () {
  if [ "${BASH_PATH}" != "" ]
  then
    addToBashRC "export PATH=${BASH_PATH}:\$PATH"
  fi
}

addJavaHomeToBashRC () {
  addToBashRC "export JAVA_HOME=/sw/java/sdk/Sun/jdk1.6.0_17-i586/"
  addToPath "\$JAVA_HOME/bin/"
}

addHomeBinToPath () {
  addToPath "\$HOME/bin"
}

## TODO(jwilson): Fix this once patch is available on unicorn.
patchSettingsXml () {
##  dest="${TMP_DIR}/settings.xml.patch"
  dest="${HOME_BIN}/atlassian-plugin-sdk/apache-maven/conf/settings.xml"

  log "Patching settings.xml to add proxy information"

##  getFile "${dest}" "${SETTINGS_XML_PATCH}"
##  patch "${HOME_BIN}/atlassian-plugin-sdk/apache-maven/conf/settings.xml" < "${dest}"

  getFile "${dest}" "${SETTINGS_XML_PROXY}"
}

installAtlassianPluginSDK () {
  dest="${TMP_DIR}/atlassian-plugin-sdk"

  log "Installing the Atlassian Plugin SDK version ${ATL_PLUGIN_SDK_VERSION}"

  qpushd ${TMP_DIR}
  
  getFile "${dest}.tar.gz" "${ATL_PLUGIN_SDK}"
  untarGz ${dest}
  
  mv "atlassian-plugin-sdk-${ATL_PLUGIN_SDK_VERSION}" "${HOME}/bin"
  ln -s "${HOME_BIN}/atlassian-plugin-sdk-${ATL_PLUGIN_SDK_VERSION}" "${HOME_BIN}/atlassian-plugin-sdk"

  addToPath "\$HOME/bin/atlassian-plugin-sdk/bin"
  addToPath "\$HOME/bin/atlassian-plugin-sdk/apache-maven/bin"

  patchSettingsXml

  log "Setup atlassian Plugin SDK at ${HOME}/bin/atlassian-plugin-sdk"

  qpopd
}

cleanupTmp () {
  rm -rf "${TMP_DIR}"
}

installApache () {
  /sw/install/services/sys_apache/install.sh
}

cleanupApache () {
  apacheDir=/data/sys_apache

  log "Cleaning up intial apache vhosts configuration"

  rm ${apacheDir}/vhosts.d/signer.localhost.conf
  rm ${apacheDir}/vhosts.d/instance.jirastudio.com-ssl.conf
  rm ${apacheDir}/vhosts.d/includes/instance.jirastudio*.conf
}

setupApacheSample () {
  apacheDir=/data/sys_apache

  log "Creating sample apache vhosts configuration"

  sample_dest="${TMP_DIR}/instance.sample.conf"
  sample_includes_dest="${apacheDir}/vhosts.d/includes/instance.${HOSTNAME}.conf"

  getFile ${sample_dest} ${APACHE_SAMPLE_CONF}
  getFile ${sample_includes_dest} ${APACHE_INCLUDE_SAMPLE_CONF}

  cat ${sample_dest} | sed -e "s/##HOSTNAME##/${HOSTNAME}/" > "${apacheDir}/vhosts.d/instance.${HOSTNAME}.conf"

  mkdir -p /data/www

  getFile /data/www/index.html "${SAMPLE_INDEX_HTML_FILE}"

  svc -t /service/sys_apache/
}

setupCheckUserScript () {
  getFile "${HOME_BIN}/checkuser.sh" ${CHECK_USER_SCRIPT}
}

setupUnicornServiceScript () {
  getFile "${HOME_BIN}/unicorn-service" ${UNICORN_SERVICE_SCRIPT}
  chmod +x ${HOME_BIN}/unicorn-service
}

setupInstallDir () {
  mkdir -p "${HOME_BIN}"
  mkdir -p "${TMP_DIR}"
}

touch "${INSTALL_CHECK_FILE}"

setupInstallDir

run "addJavaHomeToBashRC"
run "addHomeBinToPath"
run "installAtlassianPluginSDK"
run "installApache"
run "cleanupApache"
run "setupApacheSample"
run "setupCheckUserScript"
run "setupUnicornServiceScript"

## This should be last to update the path and bash in one go.
addPathToBashRC
commitToBashRC

cleanupTmp

log ""
log "============================================================"
log "================== UNICORN SETUP COMPLETE =================="
log "============================================================"
